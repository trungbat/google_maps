from django.urls import path
from django.views.generic.edit import UpdateView
from . import views


urlpatterns = [

	path('chothue/', views.NhaListView, name = 'nha_list'),
	path('create/', views.NhaCreateview ,  name = 'dangnha'),
	path('nha/<int:pk>/edit/', views.EditNhaView.as_view(), name = 'sua'),
	path('nha/tim-kiem/', views.SearchView, name= 'tim_nha'),
	path('develop/', views.DevelopView, name = 'develop'),
	path('devete/<int:pk>/', views.DeleteNhaview, name = 'delete_nha'),
	path('xe/', views.XeRegisterView, name = 'xe_register'),
	path('ajax/createcomment/<int:id>/', views.commentView, name = 'create_comment')
]

