from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from django.views.generic.edit import UpdateView
from .forms import NhaCreateForm, SearchForm, XeForm,CommentForm
from .models import Nha, Images, Xe, ChuyenDo
from django.core import serializers
from django.forms import modelformset_factory
import geopy.distance
from django import forms
from django.http import JsonResponse

from django.contrib import messages
from django.template import RequestContext


@login_required
def NhaListView(request):
	nhas = Nha.objects.filter(author = request.user)
	# nha_data = list(Nha.objects.values_list('description','price','category','phone_number','address','lat','lng'))
	nha_data = serializers.serialize('json', nhas)
	return render(request,'mymap/nha_list.html',
					{'nhas':nhas, 'nha_data':nha_data,'section':'list'}
				)

@login_required
def NhaCreateview(request):
	
	ImageFormSet = modelformset_factory(Images,
										fields=('image',),
										extra=4)
	
	if request.method == 'POST':
		form = NhaCreateForm(data=request.POST)
		formset = ImageFormSet(request.POST or None, request.FILES or None)
		if form.is_valid() and formset.is_valid():

			nha = form.save(commit=False)
			nha.author = request.user
			nha.save()

			data = Images.objects.filter(nha= nha)
			for index, f in enumerate(formset):
				if f.cleaned_data:
					if f.cleaned_data['id'] is None:
						photo = Images(nha = nha, image =f.cleaned_data['image'])
						photo.save()
					else:
						photo = Images(nha = nha, image =f.cleaned_data['image'])
						d = Images.objects.get(id=data[index].id)
						d.image = photo.image
						d.save()
			
			return redirect('nha_list')
			
	else:
		form = NhaCreateForm(initial={"phone_number": request.user.phone_number,
								'address':request.user.address,
								'lat':request.user.lat,
								'lng':request.user.lng,
							})
		formset = ImageFormSet(queryset =Images.objects.none())
	
	toa_do = [request.user.lat, request.user.lng]
	
	return render(request, 'mymap/nha_register.html',{'form':form, 'section':'create', 'toa_do':toa_do, 'formset':formset})
	

class EditNhaView(UpdateView):
	model = Nha
	form_class = NhaCreateForm
	# fields = ['description', 'category',  'price', 'phone_number', 'address', 'lat', 'lng']
	template_name = 'mymap/nha_edit.html'
	


def ketQuaTimKiem(user_latlng, category,typeroom, price, id_list,xe_latlng,xe_id_list,khoangcach):
	nha_latlng = []
	data = Nha.objects.filter(category = category,typeroom = typeroom, price__lte = price).values_list('lat','lng','id')
	xe_data = Xe.objects.values_list('lat','lng','id')

	for i in data:
		mo = [float(i[0]) , float(i[1])]

		if (geopy.distance.vincenty(mo, user_latlng).km <3.0):
			khoangcach.append(round(geopy.distance.vincenty(mo, user_latlng).km,2))

			nha_latlng.append(mo)
			id_list.append(int(i[2])) 

	for xe in xe_data:
		xe_mo = [float(xe[0]),float(xe[1])]
		if (geopy.distance.vincenty(xe_mo, user_latlng).km <3.0):
			xe_latlng.append(xe_mo)
			xe_id_list.append(int(xe[2])) 


	return nha_latlng


def commentView(request,id ):
	nha = get_object_or_404(Nha, id=id)
	user = nha.author
	data = {}
	new_comment= None
	comment_form = CommentForm(data= request.POST)
	# print(request.method)
	if comment_form.is_valid():
		# print('truuuuuuuuu')
		new_comment = comment_form.save(commit = False)
		new_comment.body = comment_form.cleaned_data['body']
		# print(new_comment.body)
		new_comment.post = nha 
		new_comment.save()
		data['is_valid'] = True
		comments = nha.comments.filter(active= True)
		context = {'form':comment_form,'comments':comments,'id_nha':nha.id , 'user':user}
		data['html_form']  = render_to_string('mymap/comment_create.html',context,request= request)
		return JsonResponse(data)
 
	else:
		comment_form = CommentForm()
		messages.error(request,'Hãy nhận xét gì đó');
		data['is_valid'] = False

	comment_form = CommentForm()

	comments = nha.comments.filter(active= True)
	context = {'form':comment_form,'comments':comments,'id_nha':nha.id , 'user':user}
	data['html_form']  = render_to_string('mymap/comment_create.html',context,request= request)

	return JsonResponse(data)

@login_required
def SearchView(request):
	xe_latlng = []
	id_list = []
	khoangcach = []
	xe_id_list = []
	toa_do = [0,0]
	nha_data = None
	xe_data = None
	is_error = 1

	if request.method == 'POST':
		form = SearchForm(data=request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			category = cd['category']
			typeroom = cd['typeroom']
			price = cd['price']
			user_latlng = [cd['lat'], cd['lng']]
			toa_do = [float(cd['lat']),float(cd['lng'])]

			toa_do_list = ketQuaTimKiem(user_latlng, category,typeroom,price, id_list,xe_latlng,xe_id_list,khoangcach)
			nhas = Nha.objects.filter(id__in = id_list)
			xes = Xe.objects.filter(id__in = xe_id_list)
			# print(len(toa_do_list))
			# print(id_list)
			nha_data = serializers.serialize('json', nhas)
			xe_data = serializers.serialize('json',xes)
			# for nha in nhas:
			# 	print(nha.images.first())
			nha_images = dict()
			for x,nha in enumerate(nhas):
				nha_images[x]  = []
				for img in nha.images.all():
					nha_images[x].append(img.image.url)

			is_error = form.cleaned_data['lat']
			return render(request,'mymap/tim_nha.html', {'form':form, 'toa_do_list':toa_do_list, 'dem':len(toa_do_list), 'nha_data':nha_data, 'nhas':nhas,'section':'search', 'toa_do':toa_do, 'xe_data':xe_data,'khoangcach':khoangcach, 'is_error':is_error})


		
	else:
		form = SearchForm(initial={
			'lat':request.user.lat,
			'lng':request.user.lng,})
		
		toa_do = [request.user.lat, request.user.lng]
		xes= Xe.objects.all()
		xe_data = serializers.serialize('json',xes)

	return render(request, 'mymap/tim_nha.html', {'form':form, 'toa_do_list': None, 'nha_data':nha_data, 'section':'search', 'toa_do':toa_do, 'xe_data':xe_data,'khoangcach':khoangcach,'is_error':is_error})


def DevelopView(request):
	return render(request, 'develop.html')

@login_required
def DeleteNhaview(request,pk):
	nha  = get_object_or_404(Nha, pk =pk)
	nha.delete()

	return redirect('nha_list')


@login_required
def XeRegisterView(request):		
	toa_do = []
	if request.method == 'POST':
		form = XeForm(request.POST)
		if form.is_valid():
			Xe.objects.filter(author = request.user).delete()
			xe = form.save(commit = False)
			xe.author = request.user
			xe.save()
			messages.success(request,'Đăng ký thành công!')
			moto = Xe.objects.filter(author = request.user).first()
			toa_do.extend([float(moto.lat), float(moto.lng)])

		else:
			messages.error(request, 'Phải chọn vị trí trên map hoặc thêm sô điện thoại')
			
	else:
		moto = Xe.objects.filter(author= request.user).first()
		# toa_do.extend([float(moto.lat) or 21.011345024530100, float(moto.lng) or 105.848035812378000])
		if moto:
			form = XeForm(initial={	
									"name": moto.name or request.user.username,
									"phone_number": moto.phone_number or request.user.phone_number,
									'lat':moto.lat or 21.011345024530100,
									'lng':moto.lng or 105.848035812378000,
								})

		else:
			form = XeForm(initial={	
									"name": request.user.username,
									"phone_number": request.user.phone_number,
									'lat':request.user.lat or 21.011345024530100,
									'lng':request.user.lng or 105.848035812378000,
								})
			

	return render(request, 'mymap/xe_register.html', {'form':form, 'section':'xe', 'toa_do':toa_do})

