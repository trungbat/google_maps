from django import forms
from .models import Nha, ChuyenDo, Xe,Comment
from django.views.generic.edit import UpdateView
from django.forms import Textarea

class NhaCreateForm(forms.ModelForm):
	CATEGORY_CHOICES = (
		('rc','Riêng chủ'),
		('cc','Chung chủ'),
	)
	ROOM_CHOICES = (
		('nt','Nhà trọ'),
		('no','Chung cư/Nhà ở'),
	)

	description = forms.CharField(widget = forms.Textarea(attrs={'class': 'textarea is-primary', 'placeholder': 'Miêu tả phòng...','rows':"3"}))
	category = forms.CharField(widget = forms.Select(attrs= {'class':'input '}, choices=CATEGORY_CHOICES))
	typeroom = forms.CharField(widget = forms.Select(attrs= {'class':'input '}, choices=ROOM_CHOICES))
	phone_number  = forms.CharField(label =('Số điện thoại'), widget = forms.TextInput(attrs = {'class':'input is-primary','placeholder':'Bắt buộc'}))
	address  = forms.CharField(label =('Địa chỉ'), widget = forms.TextInput(attrs = {'class':'input is-primary', 'placeholder':'Số .. đường ..'}))
	lat = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lat'}))
	lng = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lng'}))
	price = forms.DecimalField(widget = forms.NumberInput(attrs = {'id':'demo','class':'input'}) ,max_digits=5, decimal_places=3)
	

	class Meta:
		model = Nha 
		fields  = ('description','price','category','typeroom','phone_number','address','lat','lng')




class SearchForm(forms.ModelForm):
	CATEGORY_CHOICES = (
		('rc','Riêng chủ'),
		('cc','Chung chủ'),
	)
	ROOM_CHOICES = (
		('nt','Nhà trọ'),
		('no','Chung cư/Nhà ở'),
	)
	
	lat = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lat'}))
	lng = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lng'}))
	price = forms.DecimalField(widget = forms.NumberInput(attrs = {'id':'demo','class':'input'}) ,max_digits=5, decimal_places=3)
	category = forms.CharField(widget = forms.Select(attrs= {'class':'input '}, choices=CATEGORY_CHOICES))
	typeroom = forms.CharField(widget = forms.Select(attrs= {'class':'input '}, choices=ROOM_CHOICES))
	

	class Meta:
		model = Nha 
		fields = ('category','typeroom','price','lat','lng')


class XeForm(forms.ModelForm):
	
	class Meta:
		model = Xe 
		fields = ('name','phone_number','lat','lng',)

# # class ChuyenDoForm(forms.Form):

# 	lat = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lat_cd'}))
# 	lng = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lng_cd'}))
	
# 	class Meta: 
# 		model = ChuyenDo
# 		fields = '__all__'

class CommentForm(forms.ModelForm):
	class Meta:
		model = Comment
		fields = ('body',)
		widgets = {
            'body': Textarea(attrs={'cols':100,'rows':4,'class':'textarea','placeholder':'Bình luận'}),
        }
