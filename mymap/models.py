from django.db import models
from django.conf import settings
from django.utils import timezone
from django.core.validators import RegexValidator
from django.urls import reverse
import json

class Nha(models.Model):
	CATEGORY_CHOICES = (
		('cc','Chung chủ'),
		('rc','Riêng chủ'),
	)
	ROOM_CHOICES = (
		('nt','Nhà trọ'),
		('no','Chung cư/Nhà ở'),
	)

	author = models.ForeignKey(settings.AUTH_USER_MODEL,
							related_name= 'tao_nha',
							on_delete= models.CASCADE)

	description = models.TextField(('Miêu tả'),)
	price = models.DecimalField(('Giá phòng'),max_digits=5, decimal_places=3)
	category = models.CharField(('Loại phòng'),max_length = 15, choices = CATEGORY_CHOICES, default = 'rieng')
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Điện thoại phải ở định dạng: '+999999999'. Chỉ chứ tối đa 15 số.")
	phone_number = models.CharField(('Số điện thoại') ,validators=[phone_regex], max_length=17, blank=True) 
	address = models.CharField(('Địa chỉ'), max_length = 100)
	lat = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True)
	lng = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True)
	publish = models.DateTimeField(default=timezone.now)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	typeroom = models.CharField(('Kiểu phòng'),max_length = 15, choices = ROOM_CHOICES, default = 'nt')

	class Meta:
		ordering = ('-publish',)
		verbose_name_plural = 'Nhà'

	def get_absolute_url(self):
		return reverse('nha_list')
 

	def __str__(self):
		return self.description

class Images(models.Model):

	nha = models.ForeignKey(Nha, verbose_name = 'Nhà',related_name='images', on_delete = models.CASCADE)
	image = models.ImageField(upload_to = 'images/', verbose_name = 'Hình ảnh', blank=True, null= True)

	def __str__(self):
		return self.nha.description[:15] +'Image'

class Xe(models.Model):
	
	author = models.OneToOneField(settings.AUTH_USER_MODEL, related_name = 'xe',
							on_delete= models.CASCADE)

	name = models.CharField(('Tên chủ xe'), max_length = 25)
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Điện thoại phải ở định dạng: '+999999999'. Chỉ chứ tối đa 15 số.")
	phone_number = models.CharField(('Số điện thoại') ,validators=[phone_regex], max_length=17, blank=True) 
	lat = models.DecimalField(max_digits = 18, decimal_places = 15)
	lng = models.DecimalField(max_digits = 18, decimal_places = 15)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name_plural = 'Xe chở đồ'

class ChuyenDo(models.Model):
	name = models.CharField(('Tên tổ chức'), max_length = 25)
	description = models.TextField(('Giới thiệu'),)
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Điện thoại phải ở định dạng: '+999999999'. Chỉ chứ tối đa 15 số.")
	phone_number = models.CharField(('Số điện thoại') ,validators=[phone_regex], max_length=17, blank=True) 
	lat = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True)
	lng = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True)

	def __str__(self):
		return self.name

	class Meta:
		# ordering = ('-publish',)
		verbose_name_plural = 'DỊch vụ chuyển đồ'

class Comment(models.Model):

	post  = models.ForeignKey(Nha, on_delete= models.CASCADE, related_name= 'comments')

	body  = models.TextField(('Bình luận'), blank=True)
	created = models.DateTimeField(('Ngày viết'), auto_now_add=True )
	updated = models.DateTimeField(('Đã chỉnh sửa'),auto_now = True)
	active = models.BooleanField(default = True)

	class Meta:
		ordering = ('created',)

	def __str__(self):
		return 'Bình luận bởi {}'.format(self.post)

