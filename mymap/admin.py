from django.contrib import admin
from .models import Nha, ChuyenDo, Xe,Comment

class NhaAdmin(admin.ModelAdmin):
	list_display = ('description','price','category','address')
	list_filter  = ('category','created','publish')
	date_hierarchy = 'publish'
	ordering = ('price','publish')

admin.site.register(Nha, NhaAdmin)

class XeAdmin(admin.ModelAdmin):
	list_display = ('name','phone_number',)
	list_filter  = ('name',)
	# date_hierarchy = 'publish'
	ordering = ('name',)

admin.site.register(Xe, XeAdmin)

class ChuyenDoAdmin(admin.ModelAdmin):
	list_display = ('name','description','phone_number',)
	list_filter  = ('name',)
	# date_hierarchy = 'publish'
	ordering = ('name',)

admin.site.register(ChuyenDo, ChuyenDoAdmin)

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
	list_display = ('post','created','active',)
	list_filter = ('active','created','updated',)
	search_fields = ('body',)