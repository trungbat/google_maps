from django import template

register = template.Library()


dict_time = {'day':'ngày','hours':'giờ','minutes':'phút','weeks':'tuần','month':'tháng'}
@register.filter
def to_time(value):
	value = str(value)
	result = ''
	for anh,viet in dict_time.items():
		result = value.replace(anh, viet)
		value = result
	return result
