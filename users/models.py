from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
	
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Điện thoại phải ở định dạng: '+999999999'. Chỉ chứ tối đa 15 số.")
	phone_number = models.CharField(('Số điện thoại'),validators=[phone_regex], max_length=17, blank=True) 
	address = models.CharField(('Địa chỉ'), max_length = 100)

	lat = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True, default = 21.011345024530100)
	lng = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True, default = 105.848035812378000)

	def __str__(self):
		return '{}'.format(self.username)
