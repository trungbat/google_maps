# users/views.py
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic.edit import UpdateView
from .forms import CustomUserCreationForm, CustomUserChangeForm

class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
 