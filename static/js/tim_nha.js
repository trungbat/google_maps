$(function(){
	
	var comment  ;
	$('a.click_comment').on('click', function(e){
		e.preventDefault();
		var cmt = $(this);

		$.ajax({
			url: cmt.attr('data-url'),
			type:'get',
			dataType: 'json',
			success: function(data){
				$("#post_comment").html(data.html_form);
				$('label[for="id_body"]').hide();
				$('form').on('submit',function(e){
					e.preventDefault();

					$.ajax({
						url: $(this).attr('data-url'),
						type: 'get',
						dataType: 'json',
						success: function(data){
							if (data.is_valid){
								$("#post_comment").html(data.html_form);
								$('label[for="id_body"]').hide();
							}
				
						}
					})
					
				});
			}
		})

	});


	var homes = $('tr#home_search'), homes_hide = $('tr:hidden');
	var loadmore = $('#loadmore');

	homes.hide();
	if ($('tr:hidden').length == 0){
			loadmore.hide();
		};

	homes.slice(0,2).show();
	loadmore.on('click', function(e){

		e.preventDefault();

		$('tr:hidden').slice(0,2).slideDown();

		if ($('tr:hidden').length == 0){
			loadmore.fadeOut('slow');
		};


	});



	// var x = $('b[class^="image-"]');

	// alert(x.length);


})