L.MakiMarkers.accessToken = "pk.eyJ1IjoidHJ1bmdiYXQiLCJhIjoiY2ptdWllZTY4MDlhNjNwcGtqY2FjaGtkeCJ9.F51_3qvVuNyo_C17deJb5A"

var map = L.map('map').setView([20.9894757,105.8523328], 8);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);


var url_icon = document.getElementById('url_icon').value 
var myIcon = L.icon({

    iconUrl: url_icon,
    iconSize: [50, 50],
    shadowSize: [20, 40],
    iconAnchor: [25, 45],
    shadowAnchor: [20, 40],
    popupAnchor: [0, -53]

});

var searchControl = L.esri.Geocoding.geosearch().addTo(map);
var results = L.layerGroup().addTo(map);
searchControl.on('results', function(data){
  
    results.clearLayers();
    for (var i = data.results.length - 1; i >= 0; i--) {

       var m1 = L.marker(data.results[i].latlng, {icon: myIcon, draggable:true})
       results.addLayer(m1);

        m1.on('drag', function(e){
          document.getElementById('lat_xe').value = e.latlng['lat'];
          document.getElementById('lng_xe').value = e.latlng['lng']
        })
    }

  });

// var toa_do = {{toa_do|safe}};

// alert('Hello')


